
#Проверяет, является ли число простым. Выводит True если да, False если нет
def is_prime(x):
	"""Определяет, является ли число простым;
		x - целое положительное число
	"""
	divisor = 2
	while divisor < x:
		if x%divisor == 0:
			return False
		divisor += 1
	return True

#Раскладывает число на множители и выводит на экран
def factorize_number(x):
	"""Разложение числа на множители
		Печатает их на экран
	"""
	divisor = 2
	while x > 1:
		if x%divisor == 0:
			print(divisor)
			x //= divisor
		else:
			divisor += 1

#Находит факториал заданного натурального числа
def factorial(n):
	assert n>=0 , "ERROR"
	if n == 0:
		return 1
	return factorial(n-1)*n

#Находит наибольший общий делитель двух чисел. Оптимизированный алгоритм Евклида
def gcd(a,b):
	if b == 0:
		return a 
	return gcd(b, a%b)

#Медленный способ возведения в степень натурального числа в натуральную степень
def power_slow(a,n):
	assert n >= 0 , "ERROR" 
	if n == 0:
		return 1
	return power_slow(a,n-1)*a

#Оптимизированный способ возведения в степень натурального числа в натуральную степень
def power(a,n):
	assert n >= 0 , "ERROR" 
	if n == 0:
		return 1
	elif n%2 ==1:
		return power(a,n-1)*a
	else:
		return power(a*a,n//2)

#СУПЕР медленный и плохой способ нахождения чисел фибоначи. Не используй его!!!
def fibonacci_slow(n):
	if n == 1 or n == 2:
		return 1
	return fibonacci(n-1) + fibonacci(n-2)

#Используй эту функцию нахождения чисел фибоначи, она топчик
def fibonacci(n):
	fib = [0,1] + [0]*(n-1)
	for i in range(2, n+1):
		fib[i] = fib[i-1]+fib[i-2]
	return fib[n-1]

#Посчитай минимальную стоимость прыжков кузнечика с командами +1 и +2
def count_min_cost(N, price:list):
	C = [float("-inf"), price[1], price[1]+price[2]]+[0]*(N-2)
	for i in range(3,N+1):
		C[i] = price[i] + min(C[i-1],C[i-2])
	return C[N]

#Классная сортировка методом Тони Хоара. Средняя сложность O(logN), но может дойти и до O(N**2) в самом худшем случае
def hoar_sort(A):
	""" Сортировка методом Тони Хоара,
		Quick Sort
	"""
	if len(A) <= 1:
		return
	barrier = A[0]
	L = []
	M = []
	R = []
	for x in A:
		if x < barrier:
			L.append(x)
		elif x == barrier:
			M.append(x)
		else:
			R.append(x)
	k = 0
	hoar_sort(L)
	hoar_sort(R)
	for x in L+M+R:
		A[k] = x
		k += 1
	
#Слияние двух массивов в порядке возрастания. Нужно, в основном, только для сортировки слиянием
def merge(A: list, B: list):
	C = [0]*(len(A)+len(B))
	i = k = n = 0
	
	while i < len(A) and k < len(B):
		if A[i] < B[k]:
			C[n] = A[i]
			i += 1
			n += 1
		else:
			C[n] = B[k]
			k += 1
			n += 1
	while i < len(A):
		C[n] = A[i]
		i += 1
		n += 1
	while k < len(B):
		C[n] = B[k]
		k += 1
		n += 1
	return C
		
#Сортировка слиянием. Тоже классная, но требует дополнительной памяти. Сложность O(logN)
def merge_sort(A):
	"""Сортировка методом слияния
	"""
	if len(A) <= 1:
		return
	middle = len(A)//2
	L = [A[i] for i in range(0,middle)]
	R = [A[i] for i in range(middle, len(A))]
	merge_sort(L)
	merge_sort(R)
	C = merge(L,R)
	for i in range(len(A)):
		A[i] = C[i]

#Выводит левый и правый индексы чисел между ним. 	

#Левая граница бинарного поиска
def left_bound(A, key):
	left = -1
	right = len(A)
	while right - left > 1:
		middle = (left+right)//2
		if A[middle] < key:
			left = middle
		else:
			right = middle
	return left

#Правая граница бинарного поиска
def right_bound(A, key):
	left = -1
	right = len(A)
	while right - left > 1:
		middle = (left+right)//2
		if A[middle] <= key:
			left = middle
		else:
			right = middle
	return right
		
# - минимальное редакционное расстояние, необходимое для превращения из одной строки в другую, используя только добавление символов, удаление или изменение. Сложность O(N*M)
def levenstein(A,B):
	F = [[(i+j) if i*j == 0 else 0 for j in range(len(B)+1)]for i in range(len(A) + 1)]
	for i in range(len(A) + 1 ):
		for j in range(1,len(B)+1):
			if A[i-1] == B[j-1]:
				F[i][j] = F[i-1][j-1]
			else:
				F[i][j] = 1 + min(F[i-1][j], F[i][j-1], F[i-1][j-1])
	return F[len(A)][len(B)]
	
#Наивный алгоритм равенства строк O(N)
def equal(A,B):
	if len(A) != len(B):
		return False
	for i in range(len(A)):
		if A[i] != B[i]:
			return False
	return True
	
#Поиск подстроки в строке(наивный)
def search_substring_slow(s, sub):
	for i in range(0,len(s)-len(sub)):
		if equal(s[i : i + len(sub)], sub):
			print(i)
			
#Префикс для алгоритма Кнутта-Морриса-Пратта
def prefix(string):
	M = [0]*len(string)
	i = j = 0
	for i in range(1, len(string)):
				while j > 0 and M[i] != M[j]:
					j = M[j-1]
				if string[j] == string[i]:
					M[i] = j + 1
					j += 1
	return M

#Поиск методом Кнутта-Морриса-Пратта
def search_kmp(string, sub_string):
	""" быстрый поиск методом Кнутта-Морриса-Пратта
		Сложность - О*(N+M)
	"""
	new_string = sub_string + "#" + string
	K = prefix(new_string)
	for i in range(0, len(new_string)):
		if K[i] == len(sub_string):
			return i - len(sub_string)*2 
	return -1	
	
#Генерирует числа в N-ричной системе счисления
def generate_numbers(N,M,prefix = None):
	""" Генерирует все числа в N-ричной
		системе счисления(N <=10
	"""
        
	prefix = prefix or []
	
	if M == 0:
		print(prefix)
		return
	
	for digit in range(N):
		prefix.append(digit)
		generate_numbers(N,M-1,prefix)
		prefix.pop()
	
#Ищет число в листе
def find(number,A):
	""" Ищет x в А и возвращает True если такой есть
		False, если такого нет
	"""
	for x in A:
		if number == x:
			return True
	return False

#Создает перестановки N чисел в M позициях
def generate_permutations(N,M=-1,prefix = None):
	""" Генерирует перестановки N чисел в M позициях,
		с префиксом prefix
	"""
	M = N if M == -1 else M
	prefix = prefix or []
	if M == 0:
		print(*prefix, end=", ", sep="")
		return
	for number in range(1,N+1):
		if find(number,prefix):
			continue
		prefix.append(number)
		generate_permutations(N, M-1, prefix)
		prefix.pop()

#Переворачивание массива
def invert_array(A, N):
	""" Обращение массива (поворот задом-наперёд)
		в рамках индексов от 0 до N-1
	"""
	for k in range(N//2):
		A[k],A[N-1-k] = A[N-1-k],A[k]
	
#Смещение массива со сдвигом влево
def cycle_copy_left(A,N):
	tmp = A[0]
	for k in range(N-1):
		A[k] = A[k+1]
	A[N-1] = tmp

#Смещение массива со сдвигом вправо
def cycle_copy_right(A,N):
	tmp = A[N-1]
	for k in range(N-1,0,-1):
		A[k] = A[k-1]
	A[0] = tmp
	
#Largest common subsequence - наибольшая общая подпоследовательность
def lcs(A,B):
	F = [[0]*(len(B) + 1) for i in range(len(A) + 1)]
	for i in range(1, len(A) + 1):
		for j in range(1, len(B) + 1):
			if A[i-1] == B[j-1]:
				F[i][j] = 1 + F[i-1][j-1]
			else:
				F[i][j] = max(F[i-1][j], F[i][j-1])
	return F[-1][-1]

#Наибольшая возрастающая подпоследовательность - нужно отсортировать данную подпоследовательность и найти наибольшую общую подпоследовательность между ними	
def las(A):
	B = []
	for i in range(len(A)):
		B.append(A[i])
	hoar_sort(B)
	return lcs(A,B)
		
#Структура данных - стэк:

import A_stack
	
#Проверка корректности скобочной последовательности
def parenthesis(string):
	"""
	Проверяет скобочное выражение
	
	>>> parenthesis("((()))")
	True
	>>> parenthesis("][")
	False
	>>> parenthesis("[(()])")
	False
	"""
	for brace in string:
		if brace not in "()[]":
			continue
		if brace in "([":
			A_stack.push(brace)
		else:
			assert brace in ")]", "Катастрофа! Ожидалась закрывающаяся скобка:" + str(brace)
			if A_stack.is_empty():
				return False
			left = A_stack.pop()
			assert left in "([", "Катастрофа! Ожидалась открывающая скобка:" + str(left)
			if left == "(":
				right = ")"
			elif left == "[":
				right = "]"
			if right != brace:
				return False
	return A_stack.is_empty()
	
#Обратная польская нотация. Алгоритм вычисления выражений в постфиксной нотации
def polish_notation(A):
	"""
	Обратная польская нотация. Алгоритм вычисления выражений в постфиксной нотации,
	На вход получает лист с числами и арифметическими действиями
	>>> polish_notation([2,7,"+",5,"*"])
	45
	>>> polish_notation([2,7,5,"*","+"])
	37
	>>> polish_notation([3,4,10,"-",20,"*","-"])
	117
	
	"""
	for k in A:
		if isinstance(k, int):
			A_stack.push(k)
		elif k in "+-*/":
			x = A_stack.pop()
			y = A_stack.pop()
			if k == "+":
				z = x + y
			if k == "-":
				z = x - y
			if k == "*":
				z = x * y
			if k == "/":
				z = x // y
			A_stack.push(z)
		else:
			continue
	return A_stack.pop() if A_stack.is_empty else "Случилось какое-то говно"

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
