import A_stack
	
#Проверка корректности скобочной последовательности
	
def parenthesis(string):
	"""
	Проверяет скобочное выражение
	
	>>> parenthesis("((()))")
	True
	>>> parenthesis("][")
	False
	>>> parenthesis("[(()])")
	False
	"""
	for brace in string:
		if brace not in "()[]":
			continue
		if brace in "([":
			A_stack.push(brace)
		else:
			assert brace in ")]", "Катастрофа! Ожидалась закрывающаяся скобка:" + str(brace)
			if A_stack.is_empty():
				return False
			left = A_stack.pop()
			assert left in "([", "Катастрофа! Ожидалась открывающая скобка:" + str(left)
			if left == "(":
				right = ")"
			elif left == "[":
				right = "]"
			if right != brace:
				return False
	return A_stack.is_empty()
				
#Обратная польская нотация. Алгоритм вычисления выражений в постфиксной нотации
	
def polish_notation(A):
	"""
	Обратная польская нотация. Алгоритм вычисления выражений в постфиксной нотации,
	На вход получает лист с числами и арифметическими действиями
	>>> polish_notation([2,7,"+",5,"*"])
	45
	>>> polish_notation([2,7,5,"*","+"])
	37
	>>> polish_notation([3,4,10,"-",20,"*","-"])
	117
	
	"""
	for k in A:
		if isinstance(k, int):
			A_stack.push(k)
		elif k in "+-*/":
			x = A_stack.pop()
			y = A_stack.pop()
			if k == "+":
				z = x + y
			if k == "-":
				z = x - y
			if k == "*":
				z = x * y
			if k == "/":
				z = x // y
			A_stack.push(z)
		else:
			continue
	return A_stack.pop() if A_stack.is_empty else "Случилось какое-то говно"
	
	
if __name__ == "__main__":
	import doctest
	doctest.testmod(verbose=True)
	
